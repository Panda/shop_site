.. title: Voronoi Snowflakes
.. slug: voronoi_snowflakes
.. date: 2020-12-08 16:59:35 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text

Snowflake-like structures made from Voronoï diagrams for those who enjoy symmetry.

If you'd like to buy one of these or ask for a commission, send a mail to <span style="color:#90a959">Nausicaa</span> : *nausicaa[at]ondin[dot]es* , or a Direct Message to my <a href="https://mastodon.art/@nausicaa" rel="me">Mastodon account</a>.

The prices are given for glossy paper prints (20x20 centimeters); if you would like a different size or a different type of support (e.g., canvas or aluminium), don't hesitate to mention it in your email. Currently, we can only ship in Europe.

<div class="row">
  <div class="column">
  <span style="color:#90a959">Snowflake 13 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs13.svg">
  <span style="color:#90a959">Snowflake 24 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs24.svg">
  <span style="color:#90a959">Snowflake 52 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs52.svg">
  <span style="color:#90a959">Snowflake 68 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs68.svg">
  <span style="color:#90a959">Snowflake 75 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs75.svg">
  <span style="color:#90a959">Snowflake 77 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs77.svg">
  <span style="color:#90a959">Snowflake 83 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs83.svg">
  <span style="color:#90a959">Snowflake 85 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs85.svg">
  <span style="color:#90a959">Snowflake 95 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs95.svg">
  <span style="color:#90a959">Snowflake 135 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs135.svg">
  <span style="color:#90a959">Snowflake 150 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs150.svg">
  <span style="color:#90a959">Snowflake 152 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs152.svg">
  <span style="color:#90a959">Snowflake 168 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs168.svg">
  <span style="color:#90a959">Snowflake 192 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs170.svg">
  </div>
  <div class="column">
  <span style="color:#90a959">Snowflake 181 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs181.svg">
  <span style="color:#90a959">Snowflake 181 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs181.svg">
  <span style="color:#90a959">Snowflake 187 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs187.svg">
  <span style="color:#90a959">Snowflake 203 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs203.svg">
  <span style="color:#90a959">Snowflake 217 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs217.svg">
  <span style="color:#90a959">Snowflake 220 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs220.svg">
  <span style="color:#90a959">Snowflake 230 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs230.svg">
  <span style="color:#90a959">Snowflake 251 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs251.svg">
  <span style="color:#90a959">Snowflake 265 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs265.svg">
  <span style="color:#90a959">Snowflake 276 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs276.svg">
  <span style="color:#90a959">Snowflake 279 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs279.svg">
  <span style="color:#90a959">Snowflake 282 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs282.svg">
  <span style="color:#90a959">Snowflake 295 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs295.svg">
  <span style="color:#90a959">Snowflake 299 (20€)</span>
  <img src="/galleries/voronoi_snowflakes/vs299.svg">
  </div>
</div>
