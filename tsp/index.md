.. title: TSP art
.. slug: tsp
.. date: 2020-12-08 16:59:35 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text

TSP stands for "Traveling Salesman Problem". It tells the story of a salesman willing to find the shortest path between all the cities he travels to, to sell his goods. Here, we draw the path he travels to reveal an image with meandering lines.

If you'd like to buy one of these or ask for a commission, send a mail to <span style="color:#90a959">Nausicaa</span> : *nausicaa[at]ondin[dot]es* , or a Direct Message to my <a href="https://mastodon.art/@nausicaa" rel="me">Mastodon account</a>.

<div class="row">
  <div class="column">
    <span style="color:#90a959">Macaw (A4, available, 30€).</span>
    <img src="/galleries/tsp/macaw.png">
    <span style="color:#90a959">Street Fashion (A3, available, 40€).</span>
    <img src="/galleries/tsp/street_fashion.jpg">


  </div>
  <div class="column">
    <span style="color:#90a959">Portrait in blue (A4, available, 30€).</span>
    <img src="/galleries/tsp/portrait_blue.png">
    <span style="color:#90a959">Cat's eyes (A5, available, 20€).</span>
    <img src="/galleries/tsp/cat_eyes.png">
    <span style="color:#90a959">Purple portrait (A4, available, 30€).</span>
    <img src="/galleries/tsp/portrait_dark_purple.png">
  </div>
</div>


Most of the original images come from [Pixabay](https://pixabay.com).
