.. title: Fractal art
.. slug: fractals
.. date: 2020-12-08 16:59:35 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text

Fractal arts use complex dynamical systems to build intricated self-similar shapes. Combined with various colorization techniques, it produces vibrant and convoluted abstract pictures. Follow me on Mastodon for more art and experiments: <a href="https://mastodon.art/@digit">@digit</a>

If you'd like to buy one of these, contact <span style="color:#90a959">digit</span>: *lezardidealiste[at]gmail[dot]com*. This page gives prices for glossy paper prints (20x20 centimeters) delivered in France (I can deliver to other Europe countries, but additional fees may apply). Bigger poster sizes or different types of support are also available:
<ul>
<li>40x40 centimeters, glossy paper: +15€</li>
<!--<li>50x50 centimeters, glossy paper: +20€</li>-->
<li>20x20 centimeters, canvas: +21€</li>
<li>40x40 centimeters, canvas: +39€</li>
<!--<li>50x50 centimeters, canvas: +46€</li>-->
<li>20x20 centimeters, aluminium: +21€</li>
<li>40x40 centimeters, aluminium: +39€</li>
<!--<li>50x50 centimeters, aluminium: +55€</li>-->
<!--<li>20x20 centimeters, plexiglass: +20€</li>
<li>40x40 centimeters, plexiglass: +45€</li>
<li>50x50 centimeters, plexiglass: +66€</li>-->
<li>others: in your email, describe the format you would like to have. I'll reply with a customized estimation.</li>
</ul>

With all prints, I'll email you a PDF document that represents formally the mathematical formula you bought. Curious? See <a href="/galleries/fractals/formal-example.png">this example</a>.

<div class="row">
  <div class="column">
    <span style="color:#90a959">Spring Koch Flake (20€).</span>
    <img src="/galleries/fractals/koch-flake1.jpg">
  </div>
  <div class="column">
    <span style="color:#90a959">Koch Galaxy (25€).</span>
    <img src="/galleries/fractals/koch-galaxy3.jpg">
  </div>
</div>
<div class="row">
  <div class="column">
    <span style="color:#90a959">Autumn Koch Tree (20€).</span>
    <img src="/galleries/fractals/koch-autumn-tree.jpg">
  </div>
  <div class="column">
    <span style="color:#90a959">Koch Eyes (25€).</span>
    <img src="/galleries/fractals/koch-eyes.jpg">
  </div>
</div>
<div class="row">
  <div class="column">
    <span style="color:#90a959">Koch Frosted (25€).</span>
    <img src="/galleries/fractals/koch-frosting-flake.jpg">
  </div>
  <div class="column">
    <span style="color:#90a959">Koch Totems (25€).</span>
    <img src="/galleries/fractals/koch-totems.jpg">
  </div>
<div class="row">
  <div class="column">
    <span style="color:#90a959">Koch Cherry Blossom (20€).</span>
    <img src="/galleries/fractals/koch-cherry-blossom.jpg">
  </div>
  <div class="column">
    <span style="color:#90a959">Koch Fishes (25€).</span>
    <img src="/galleries/fractals/koch-fishes.jpg">
  </div>
  <div class="row">
  <div class="column">
    <span style="color:#90a959">Koch Berries (25€).</span>
    <img src="/galleries/fractals/koch-berries.jpg">
  </div>
  <div class="column">
    <span style="color:#90a959">Koch Snowflake (25€).</span>
    <img src="/galleries/fractals/koch-snowflake.jpg">
  </div>
</div>
