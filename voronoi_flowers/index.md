.. title: Voronoi Flowers
.. slug: voronoi_flowers
.. date: 2020-12-08 16:59:35 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text

Flower-like structures made from Voronoï diagrams.

If you'd like to buy one of these or ask for a commission, send a mail to <span style="color:#90a959">Nausicaa</span> : *nausicaa[at]ondin[dot]es* , or a Direct Message to my <a href="https://mastodon.art/@nausicaa" rel="me">Mastodon account</a>.

The prices are given for glossy paper prints (20x20 centimeters); if you would like a different size or a different type of support (e.g., canvas or aluminium), don't hesitate to mention it in your email. Currently, we can only ship in Europe.

<div class="row">
  <div class="column">
  <span style="color:#90a959">Flower 3 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf3.svg">
  <span style="color:#90a959">Flower 5 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf5.svg">
  <span style="color:#90a959">Flower 15 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf15.svg">
  <span style="color:#90a959">Flower 19 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf19.svg">
  <span style="color:#90a959">Flower 36 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf36.svg">
  <span style="color:#90a959">Flower 43 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf43.svg">
  <span style="color:#90a959">Flower 44 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf44.svg">
  <span style="color:#90a959">Flower 58 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf58.svg">
  <span style="color:#90a959">Flower 68 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf68.svg">
  <span style="color:#90a959">Flower 75 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf75.svg">
  <span style="color:#90a959">Flower 116 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf116.svg">
  <span style="color:#90a959">Flower 127 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf127.svg">
  <span style="color:#90a959">Flower 147 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf147.svg">

  </div>
  <div class="column">
  <span style="color:#90a959">Flower 192 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf192.svg">
  <span style="color:#90a959">Flower 211 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf211.svg">
  <span style="color:#90a959">Flower 220 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf220.svg">
  <span style="color:#90a959">Flower 241 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf241.svg">
  <span style="color:#90a959">Flower 242 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf242.svg">
  <span style="color:#90a959">Flower 286 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf286.svg">
  <span style="color:#90a959">Flower 318 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf318.svg">
  <span style="color:#90a959">Flower 325 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf325.svg">
  <span style="color:#90a959">Flower 329 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf329.svg">
  <span style="color:#90a959">Flower 348 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf348.svg">
  <span style="color:#90a959">Flower 362 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf362.svg">
  <span style="color:#90a959">Flower 397 (20€)</span>
  <img src="/galleries/voronoi_flowers/vf397.svg">
  </div>
</div>
