.. title: Stippling art
.. slug: stippling
.. date: 2020-12-08 16:59:35 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text

Stippling is the art of harmoniously placing "stipples", basically dots, to form an image.

This method can produce beautiful portraits or sceneries.

If you'd like to buy one of these or ask for a commission, send a mail to <span style="color:#90a959">Nausicaa</span> : *nausicaa[at]ondin[dot]es* , or a Direct Message to my <a href="https://mastodon.art/@nausicaa" rel="me">Mastodon account</a>.

<div class="row">
  <div class="column">
    <span style="color:#90a959">Macaw (A4, available, 50€)</span>
    <img src="/galleries/stippling/macaw.png">
    <span style="color:#90a959">Dandelion (A4, available, 25€)</span>
    <img src="/galleries/stippling/dandelion.png">
    <span style="color:#90a959">Coffee Beans (A5, available, 15€)</span>
    <img src="/galleries/stippling/coffee_beans.png">
    <span style="color:#90a959">Waiting (A3, available, 35€)</span>
    <img src="/galleries/stippling/waiting.png">

  </div>
  <div class="column">
    <span style="color:#90a959">Portrait (A4, available, 25€)</span>
    <img src="/galleries/stippling/portrait_glasses.png">
    <span style="color:#90a959">Geometry meeting (A4, available, 25€)</span>
    <img src="/galleries/stippling/buildings.png">
    <span style="color:#90a959">Moon & Cat (A5, available, 15€).</span>
    <img src="/galleries/stippling/moon_cat.png">
  </div>
</div>

Most of the original images come from [Pixabay](https://pixabay.com).
