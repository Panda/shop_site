.. title: Ondine's Gallery
.. slug: index
.. date: 2020-12-05 22:01:15 UTC+01:00
.. tags:
.. category:
.. link:
.. description:
.. type: text

# Who are we ?
We are <a href="https://mastodon.art/@nausicaa" rel="me">Nausicaa</a> and <a href="https://mastodon.art/@digit" rel="me">Digit</a>, and we love art and maths and what comes in between. We make with passion images and prints, shown here. Most of them are available to buy !

# And what do we do ?

<div class="row">
  <div class="column">
  <a href="/fractals/index.html">Fractal art.</a>
  <a href="/fractals/index.html">
       <img src="/galleries/fractals/fractals.png" alt="The gallery of fractal art." width="400"/>
  </a>
  <a href="/voronoi_snowflakes/index.html">Voronoï snowflakes.</a>
  <a href="/voronoi_snowflakes/index.html">
       <img src="/galleries/voronoi_snowflakes/vs.svg" alt="The gallery of Voronoï snowflakes." width="400"/>
    </a>
    <a href="/tsp/index.html">TSP art.</a>
    <a href="/tsp/index.html">
         <img src="/galleries/tsp/tsp.png" alt="The gallery of TSP art." width="400"/>
      </a>

  </div>

  <div class="column">
  <a href="/voronoi_flowers/index.html">Voronoï flowers.</a>
  <a href="/voronoi_flowers/index.html">
       <img src="/galleries/voronoi_flowers/vf.svg" alt="The gallery of Voronoï flowers." width="400"/>
    </a>
  <a href="/stippling/index.html">Stipplings.</a>
  <a href="/stippling/index.html">
       <img src="/galleries/stippling/stippling.png" alt="The gallery of stippling art." width="400"/>
  </a>

  </div>


</div>
